package com.example.minimal_sync_adapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    public static final String TAG = "MainActivity";

    public static final String AUTHORITY = "com.android.contacts";
    public static final String ACCOUNT_TYPE = "com.crazyservice";
    public static final String ACCOUNT = "cabledog";

    private Account mAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        if (mAccount == null)
            mAccount = createSyncAccount(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public Account createSyncAccount(Context context) {

        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);

        AccountManager accountManager = (AccountManager) context
                .getSystemService(ACCOUNT_SERVICE);

        // Add the account and account type, no password or user data
        // If successful, return the Account object, otherwise report an error.

        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            Log.i(TAG, "Created account \"" + ACCOUNT + "\"");
            setStatus("ready.");
            return newAccount;

        } else {
            Log.e(TAG, "Error trying to create accout \"" + ACCOUNT + "\"");
            setStatus("failed!");
            return null;
        }
    }

    public void onSyncButtonClick(View v) {
        Log.i(TAG, "Sync now clicked!");

        // Pass the settings flags by inserting them in a bundle
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        /*
         * Request the sync for the default account, authority, and manual sync
         * settings
         */
        ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
        setStatus("synchronizing...");
    }

    private void setStatus(String status) {
        TextView statusView = (TextView) findViewById(R.id.statusView);
        if (statusView == null) {
            Log.w(TAG, "statusView is null :(");
            return;
        }
        statusView.setText(status);
    }
}
